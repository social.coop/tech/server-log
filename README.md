# server-log

Manual log of server maintenances, upgrades, and outages. Please track any changes you make here or link to a git.coop issue.